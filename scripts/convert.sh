#!/bin/bash

PCAP=./pcaps
CSV=./flows

for subdir in $PCAP/*; do
	subdir=$(basename -- "$subdir")
	mkdir -p $CSV/$subdir
	for file in $PCAP/$subdir/*; do
		file=$(basename -- "$file")
		name="${file%%.*}"
		csv_name="$name.csv"
		cicflowmeter -f "$PCAP/$subdir/$file" -c "$CSV/$subdir/$csv_name"
	done
done